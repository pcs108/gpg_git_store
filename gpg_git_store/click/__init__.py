import dataclasses
import os
import typing
from pathlib import Path

from .. import Store, StoreItem

DEFAULT_GPG_GIT_STORE = Path(os.path.expanduser("~")) / ".store"


@dataclasses.dataclass(slots=True)
class State:
    store_item_class: typing.Optional[typing.Type[StoreItem]] = None
    store: typing.Optional[Store] = None
    store_path: Path = Path(
        os.getenv("GPG_GIT_STORE", DEFAULT_GPG_GIT_STORE)
    ).absolute()
    store_required_error_message: str = "Store not found."
    git_commit_messages_for_commands: typing.Dict[str, str] = dataclasses.field(
        default_factory=dict
    )
