import functools
import typing

import click


class CommandNotRecognizedForGitCommit(Exception):
    pass


def abort_if_false(ctx: click.Context, _, value: typing.Any) -> None:
    if not value:
        ctx.abort()


def require_store(func: typing.Callable) -> typing.Callable:
    @functools.wraps(func)
    def require_store_inner(ctx: click.Context, *args, **kwargs) -> typing.Any:
        if not ctx.obj.store:
            click.echo(ctx.obj.store_required_error_message)
            ctx.abort()
        return func(ctx, *args, **kwargs)

    return require_store_inner


def save_git_changes(func: typing.Callable) -> typing.Callable:
    @functools.wraps(func)
    def save_git_changes_inner(ctx: click.Context, *args, **kwargs) -> typing.Any:
        resp = func(ctx, *args, **kwargs)
        if ctx.obj.store.gitter:
            message = ctx.obj.git_commit_messages_for_commands.get(ctx.command.name)
            if message:
                ctx.obj.store.gitter.add_changes_and_commit(message)
            else:
                raise CommandNotRecognizedForGitCommit(ctx.command.name)
        return resp

    return save_git_changes_inner
