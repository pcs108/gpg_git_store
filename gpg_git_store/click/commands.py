import click

from .. import Encrypter, Store, StoreDecrypter, StoreEncrypter
from . import helpers


@click.pass_context
@click.option("-k", "--gpg-key", default="", prompt=True)
@click.option("-g", "--git-init", default=False, prompt=True, is_flag=True)
def init(ctx: click.Context, gpg_key: str, git_init: bool) -> None:
    Store.init(
        item_class=ctx.obj.store_item_class,
        path=ctx.obj.store_path,
        gpg_id=gpg_key,
        git=git_init,
    )


@click.pass_context
@helpers.require_store
@helpers.save_git_changes
@click.option(
    "-f",
    "--force",
    is_flag=True,
    callback=helpers.abort_if_false,
    expose_value=False,
    prompt="Decrypt the store?",
)
def decrypt(ctx: click.Context) -> None:
    store_decrypter = StoreDecrypter(store=ctx.obj.store)
    store_decrypter.decrypt()
    click.echo("Store was decrypted.")


@click.pass_context
@helpers.require_store
@helpers.save_git_changes
@click.argument("gpg-key")
@click.option(
    "-f",
    "--force",
    is_flag=True,
    callback=helpers.abort_if_false,
    expose_value=False,
    prompt="Encrypt the store?",
)
def encrypt(ctx: click.Context, gpg_key: str) -> None:
    store_encrypter = StoreEncrypter(store=ctx.obj.store, encrypter=Encrypter(gpg_key))
    store_encrypter.encrypt()
    click.echo(f"Encrypted with: {gpg_key}.")


@click.pass_context
@helpers.require_store
@click.argument("cmd", nargs=-1)
def git(ctx: click.Context, cmd: str) -> None:
    if not ctx.obj.store.gitter:
        click.echo("Git is not set up with the store.")
        raise click.Abort()
    cmd = " ".join(cmd)
    ctx.obj.store.gitter.execute(cmd)
