import dataclasses
import hashlib
import json
import re
import shutil
import subprocess
import typing
from pathlib import Path

__author__ = "Patrick Schneeweis"
__docformat__ = "markdown en"
__license__ = "GPLv3+"
__title__ = "gpg_git_tools"
__version__ = "0.0.1"


@dataclasses.dataclass(slots=True)
class RelatedItems:
    store_item_class: typing.Type["StoreItem"]
    store: typing.Optional["Store"] = None

    def delete(self) -> None:
        shutil.rmtree(self.store.root)

    def get_directory_name_for_id(self, id: int) -> str:
        directory_id = f"{id}-{self.store_item_class.__name__}"
        return Store.get_file_name_for_id(directory_id)

    def __call__(self, *args, **kwargs) -> "RelatedItems":
        # Used to replace the generic field factory with a specific instance of
        # the field with the store established with the particular directory
        # assigned.
        # Without this step, all instances share the same `RelatedItem` field
        # singleton, with the same directory/files.
        return self.__class__(store_item_class=self.store_item_class, *args, **kwargs)

    def __getattr__(self, attr) -> typing.Any:
        try:
            return getattr(self.store, attr)
        except AttributeError:
            raise AttributeError(attr)


@dataclasses.dataclass(slots=True, kw_only=True)
class StoreItem:
    storage: "StoreFile"
    id: int
    name: str

    non_data_field_names: typing.List[str] = dataclasses.field(
        default_factory=lambda: [
            "storage",
            "non_data_field_names",
            "related_item_field_names",
        ]
    )
    related_item_field_names: typing.List[str] = dataclasses.field(default_factory=list)

    def __post_init__(self):
        for field in dataclasses.fields(self):
            if field.type == RelatedItems:
                self.related_item_field_names.append(field.name)
                self.non_data_field_names.append(field.name)
                related = getattr(self, field.name)
                directory_name = related.get_directory_name_for_id(self.id)
                directory_path = self.storage.path.parent / directory_name
                directory_path.mkdir(exist_ok=True)
                store = Store.from_directory(
                    directory_path,
                    item_class=related.store_item_class,
                    encrypter=self.storage.encrypter,
                )
                setattr(self, field.name, related(store=store))

    @classmethod
    def from_store_file(cls, store_file: "StoreFile") -> typing.Any:
        return cls(storage=store_file, **store_file.data)

    @property
    def data(self) -> dict:
        return {
            field_name: getattr(self, field_name)
            for field_name in self.data_field_names
        }

    @property
    def data_field_names(self) -> typing.List[str]:
        return [
            field.name
            for field in dataclasses.fields(self)
            if field.name not in self.non_data_field_names
        ]

    @property
    def related_items(self) -> typing.List[RelatedItems]:
        results = []
        for field_name in self.related_item_field_names:
            results.append(getattr(self, field_name))
        return results

    def delete(self) -> None:
        self.storage.delete()
        for related in self.related_items:
            related.delete()

    def save(self) -> None:
        self.storage.save(self.data)

    def update(self, **kwargs) -> None:
        for key, value in kwargs.items():
            setattr(self, key, value)


@dataclasses.dataclass(slots=True)
class Encrypter:
    SUBPROCESS_OPTIONS = {
        "shell": True,
        "executable": "/bin/bash",
        "capture_output": True,
    }

    DECRYPTION_COMMAND = "gpg --decrypt --armor --recipient {gpg_key} {path!r}"

    ENCRYPTION_COMMAND = (
        "gpg --output - --encrypt --armor --recipient {gpg_key} <(echo {text!r})"
    )

    gpg_key: str

    def decrypt(self, path: Path) -> str:
        command = Encrypter.DECRYPTION_COMMAND.format(
            gpg_key=self.gpg_key, path=str(path)
        )
        result = subprocess.run(command, **Encrypter.SUBPROCESS_OPTIONS)
        return result.stdout.decode()

    def encrypt(self, text: str) -> str:
        command = Encrypter.ENCRYPTION_COMMAND.format(gpg_key=self.gpg_key, text=text)
        result = subprocess.run(command, **Encrypter.SUBPROCESS_OPTIONS)
        return result.stdout.decode()


@dataclasses.dataclass(slots=True)
class Gitter:
    INIT_COMMAND = "git init"
    ADD_COMMAND = "git add ."
    COMMIT_COMMAND = "git commit -m {message!r}"

    path: Path

    @property
    def subprocess_options(self) -> dict:
        return {"shell": True, "cwd": str(self.path)}

    def init(self) -> None:
        (self.path / ".gitignore").write_text(".gpg-id")
        subprocess.run(Gitter.INIT_COMMAND, **self.subprocess_options)

    def add_changes_and_commit(self, message: str) -> None:
        subprocess.run(Gitter.ADD_COMMAND, **self.subprocess_options)
        subprocess.run(
            Gitter.COMMIT_COMMAND.format(message=message), **self.subprocess_options
        )

    def execute(self, command: str) -> None:
        subprocess.run(f"git {command}", **self.subprocess_options)


@dataclasses.dataclass(slots=True)
class StoreFile:
    path: Path
    encrypter: typing.Optional[Encrypter] = None

    @property
    def data(self) -> dict:
        if self.encrypter:
            file_data = self.encrypter.decrypt(self.path)
        else:
            file_data = self.path.read_text()
        return json.loads(file_data)

    def delete(self) -> None:
        self.path.unlink()

    def save(self, data: dict) -> None:
        if self.encrypter:
            file_data = self.encrypter.encrypt(json.dumps(data))
        else:
            file_data = json.dumps(data, indent=2)
        self.path.write_text(file_data)


@dataclasses.dataclass(kw_only=True)
class Store:
    root: Path
    item_class: typing.Type[StoreItem]
    encrypter: typing.Optional[Encrypter] = None
    gitter: typing.Optional[Gitter] = None

    def __post_init__(self):
        item_class_name = self.item_class.__name__
        snake_name = re.sub(r"(?<!^)(?=[A-Z])", "_", item_class_name).lower()
        setattr(self.__class__, f"{snake_name}s", property(lambda self: self.items))
        setattr(
            self.__class__, f"{snake_name}_list", property(lambda self: self.item_list)
        )
        setattr(self, f"create_{snake_name}", self.create_item)

    @classmethod
    def init(
        cls,
        path: Path,
        gpg_id: typing.Optional[str] = None,
        git: bool = False,
        **kwargs,
    ) -> "Store":
        path.mkdir(parents=True)
        store = cls(root=path, **kwargs)
        if gpg_id:
            store.set_gpg_id(gpg_id)
        if git:
            gitter = Gitter(path)
            gitter.init()
            gitter.add_changes_and_commit("Store created.")
        return Store.from_directory(path, **kwargs)

    @classmethod
    def from_directory(
        cls, directory: Path, encrypter: typing.Optional[Encrypter] = None, **kwargs
    ) -> "Store":
        if not directory.exists():
            raise FileNotFoundError(f"Store not found: {str(directory)}")

        gpg_id_file = directory / ".gpg-id"
        if not encrypter and gpg_id_file.exists():
            encrypter = Encrypter(gpg_id_file.read_text().strip())

        git_repo = directory / ".git"
        if git_repo.exists():
            gitter = Gitter(directory)
        else:
            gitter = None
        return cls(root=directory, encrypter=encrypter, gitter=gitter, **kwargs)

    @staticmethod
    def get_file_name_for_id(id: typing.Union[str, int]) -> str:
        return hashlib.md5(str(id).encode()).hexdigest()

    @property
    def gpg_id_file_path(self) -> Path:
        return self.root / ".gpg-id"

    @property
    def is_encrypted(self) -> bool:
        return self.gpg_id_file_path.exists()

    @property
    def max_id(self) -> int:
        if not self.items:
            return 0

        return max(self.items.keys())

    @property
    def item_file_paths(self) -> typing.List[Path]:
        return sorted(
            [
                path
                for path in self.root.iterdir()
                if path.is_file() and not path.name.startswith(".")
            ]
        )

    @property
    def items(self) -> typing.Dict[int, typing.Any]:
        items = {}
        for path in self.item_file_paths:
            store_file = StoreFile(path=path, encrypter=self.encrypter)
            item = self.item_class.from_store_file(store_file)
            items[item.id] = item
        return items

    @property
    def item_list(self) -> typing.List[typing.Any]:
        return sorted(list(self.items.values()), key=lambda i: i.id)

    def create_item(self, **data) -> typing.Any:
        data["id"] = data.get("id", self.max_id + 1)
        file_name = Store.get_file_name_for_id(data["id"])
        item_file = StoreFile(path=(self.root / file_name), encrypter=self.encrypter)
        item = self.item_class(storage=item_file, **data)
        item.save()
        return item

    def unset_gpg_id(self) -> None:
        self.gpg_id_file_path.unlink()

    def set_gpg_id(self, gpg_key: str) -> None:
        self.gpg_id_file_path.write_text(gpg_key)


@dataclasses.dataclass(slots=True)
class StoreDecrypter:
    class AlreadyDecryptedError(Exception):
        pass

    store: Store
    related_items: bool = False

    def decrypt(self) -> None:
        if not self.related_items and not self.store.is_encrypted:
            raise StoreDecrypter.AlreadyDecryptedError(str(self.store.root))

        for item in self.store.item_list:
            unencrypted_item_file = StoreFile(path=item.storage.path)
            unencrypted_item_file.save(item.data)
            for related in item.related_items:
                related_decrypter = StoreDecrypter(related.store, related_items=True)
                related_decrypter.decrypt()

        if not self.related_items:
            self.store.unset_gpg_id()


@dataclasses.dataclass(slots=True)
class StoreEncrypter:
    class AlreadyEncryptedError(Exception):
        pass

    store: Store
    encrypter: Encrypter
    related_items: bool = False

    def encrypt(self) -> None:
        if self.store.is_encrypted:
            raise StoreEncrypter.AlreadyEncryptedError(str(self.store.root))

        for item in self.store.item_list:
            encrypted_item_file = StoreFile(
                path=item.storage.path, encrypter=self.encrypter
            )
            encrypted_item_file.save(item.data)
            for related in item.related_items:
                related_encrypter = StoreEncrypter(
                    store=related.store, encrypter=self.encrypter, related_items=True
                )
                related_encrypter.encrypt()

        if not self.related_items:
            self.store.set_gpg_id(self.encrypter.gpg_key)


@dataclasses.dataclass(slots=True)
class ItemListFilter:
    items: typing.List[typing.Any]
    criteria: dict

    @classmethod
    def from_kwargs(cls, items: typing.List[typing.Any], **kwargs) -> "ItemListFilter":
        return cls(items=items, criteria=kwargs)

    @property
    def results(self) -> typing.List[typing.Any]:
        return [t for t in self.items if self._matches_criteria(t)]

    def _matches_criteria(self, item: typing.Any) -> bool:
        for key, value in self.criteria.items():
            if getattr(item, key) != value:
                return False
        return True


@dataclasses.dataclass(slots=True)
class ItemListSorter:
    items: typing.List[typing.Any]
    attribute: str
    reverse: bool = False

    @property
    def results(self) -> typing.List[typing.Any]:
        def key(item: typing.Any) -> typing.Any:
            return getattr(item, self.attribute)

        return sorted(self.items, key=key, reverse=self.reverse)
