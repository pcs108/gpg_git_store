import typing
from dataclasses import dataclass

DEFAULT_EMPTY_LIST_MESSAGE = "No items found."


@dataclass(slots=True)
class ItemPresenter:
    item: typing.Any

    @property
    def text(self) -> str:
        if self.item.completed:
            completed_marker = "X"
        else:
            completed_marker = " "

        base_text = f"{completed_marker} " f"[{self.item.id}] " f"{self.item.name} "
        return (base_text + self.custom_text).rstrip()

    @property
    def custom_text(self) -> str:
        return ""


@dataclass(slots=True)
class ItemListPresenter:
    items: typing.List[typing.Any]
    item_presenter_class: typing.Type[ItemPresenter] = ItemPresenter
    empty_list_message: str = DEFAULT_EMPTY_LIST_MESSAGE

    @property
    def text(self) -> str:
        if not self.items:
            return self.empty_list_message
        return "\n".join([self.item_presenter_class(item).text for item in self.items])
