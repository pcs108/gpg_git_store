import pytest

from gpg_git_store import RelatedItems, Store

from . import Relative


def test_related_items_get_directory_name_for_id():
    assert (
        RelatedItems(Relative).get_directory_name_for_id(12)
        == "9d479cd23c9f672c3cb448e9c9986286"
    )


def test_related_items_delete_removes_directory_of_files(created_store_path):
    related_files = created_store_path / "related"
    related_files.mkdir()
    store = Store(root=related_files, item_class=Relative)
    related = RelatedItems(store_item_class=Relative, store=store)
    related.delete()
    assert not related_files.exists()


def test_related_items_delegates_to_store_where_possible(created_store_path):
    store = Store(root=created_store_path, item_class=Relative)
    related = RelatedItems(store_item_class=Relative, store=store)
    assert related.create_item == store.create_item


def test_related_items_still_raises_error_if_attribute_does_not_exist(
    created_store_path,
):
    store = Store(root=created_store_path, item_class=Relative)
    related = RelatedItems(store_item_class=Relative, store=store)
    with pytest.raises(AttributeError):
        related.nonsense()
