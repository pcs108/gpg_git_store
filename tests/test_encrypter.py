from gpg_git_store import Encrypter

from . import TEST_DATA, FakeCompletedProcess


def test_encrypter_decrypt(mocked_subprocess_run):
    mocked_subprocess_run.return_value = FakeCompletedProcess(stdout=b"foobar")

    encrypted_text = """
-----BEGIN PGP MESSAGE-----

abc12345xyz98765
-----END PGP MESSAGE-----
"""
    encrypted_file = TEST_DATA / "fake.gpg"
    encrypted_file.write_text(encrypted_text)

    encrypter = Encrypter("hello@world")
    assert encrypter.decrypt(encrypted_file) == "foobar"

    mocked_subprocess_run.assert_called_with(
        Encrypter.DECRYPTION_COMMAND.format(
            gpg_key="hello@world", path=str(encrypted_file)
        ),
        **Encrypter.SUBPROCESS_OPTIONS
    )


def test_encrypter_encrypt(mocked_subprocess_run):
    mocked_subprocess_run.return_value = FakeCompletedProcess(stdout=b"h4x0r5")

    encrypter = Encrypter("hello@world")
    assert encrypter.encrypt("foobar") == "h4x0r5"

    mocked_subprocess_run.assert_called_with(
        Encrypter.ENCRYPTION_COMMAND.format(gpg_key="hello@world", text="foobar"),
        **Encrypter.SUBPROCESS_OPTIONS
    )
