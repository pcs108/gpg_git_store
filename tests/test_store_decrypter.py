import json

import pytest

from gpg_git_store import Store, StoreDecrypter

from . import AppStoreItem


def test_store_decrypter_decrypt_removes_gpg_id_and_replaces_encrypted_files(
    fake_encrypter, created_store_path
):
    item_data = {"id": 1, "name": "fooboo", "foo": True}

    gpg_id_file_path = created_store_path / ".gpg-id"
    gpg_id_file_path.write_text("boo@foo")

    fake_encrypter.gpg_key = "boo@foo"
    fake_encrypter.decrypted_data = json.dumps(item_data)

    item_file_path = created_store_path / "abc123"
    item_file_path.write_text("h4x0r5")

    store = Store(
        root=created_store_path, item_class=AppStoreItem, encrypter=fake_encrypter
    )
    store_decrypter = StoreDecrypter(store)
    store_decrypter.decrypt()

    assert not gpg_id_file_path.exists()
    assert json.loads(item_file_path.read_text()) == item_data


def test_store_decrypter_decrypt_ignores_gpg_id_file_if_for_related_items(
    fake_encrypter, created_store_path
):
    item_data = {"id": 1, "name": "fooboo", "foo": True}
    fake_encrypter.gpg_key = "boo@foo"
    fake_encrypter.decrypted_data = json.dumps(item_data)

    item_file_path = created_store_path / "abc123"
    item_file_path.write_text("h4x0r5")

    store = Store(
        root=created_store_path, item_class=AppStoreItem, encrypter=fake_encrypter
    )
    store_decrypter = StoreDecrypter(store, related_items=True)
    store_decrypter.decrypt()

    assert json.loads(item_file_path.read_text()) == item_data


def test_store_decryper_decrypt_raises_error_if_store_already_decrypted(
    created_store_path,
):
    store = Store(root=created_store_path, item_class=AppStoreItem)
    store_decrypter = StoreDecrypter(store)
    with pytest.raises(StoreDecrypter.AlreadyDecryptedError):
        store_decrypter.decrypt()
