from unittest.mock import call

from gpg_git_store import Gitter


def test_gitter_subprocess_options_sets_cwd_and_shell_option(created_store_path):
    gitter = Gitter(created_store_path)
    assert gitter.subprocess_options == {"cwd": str(created_store_path), "shell": True}


def test_gitter_init(mocked_subprocess_run, created_store_path):
    mocked_subprocess_run.side_effect = lambda _, **__: (
        created_store_path / ".git"
    ).mkdir()
    gitter = Gitter(created_store_path)
    gitter.init()
    assert (created_store_path / ".git").exists()
    assert (created_store_path / ".gitignore").read_text() == ".gpg-id"

    mocked_subprocess_run.assert_called_with(
        Gitter.INIT_COMMAND, **gitter.subprocess_options
    )


def test_gitter_add_changes_and_commit(mocked_subprocess_run, created_store_path):
    gitter = Gitter(created_store_path)
    gitter.add_changes_and_commit("i am being commited!")
    mocked_subprocess_run.assert_has_calls(
        [
            call(Gitter.ADD_COMMAND, **gitter.subprocess_options),
            call(
                Gitter.COMMIT_COMMAND.format(message="i am being commited!"),
                **gitter.subprocess_options
            ),
        ]
    )


def test_gitter_execute_runs_arbitrary_git_command(
    mocked_subprocess_run, created_store_path
):
    gitter = Gitter(created_store_path)
    gitter.execute("push -u origin main")
    mocked_subprocess_run.assert_called_with(
        "git push -u origin main", **gitter.subprocess_options
    )
