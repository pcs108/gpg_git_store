from gpg_git_store import presenters

from . import FakeItem


def test_item_presenter_for_completed_item():
    item = FakeItem(id=1, name="hello", completed=True)
    presenter = presenters.ItemPresenter(item)
    assert presenter.text == "X [1] hello"


def test_item_presenter_for_incomplete_item():
    item = FakeItem(id=2, name="foo", completed=False)
    presenter = presenters.ItemPresenter(item)
    assert presenter.text == "  [2] foo"


def test_item_present_with_custom_text():
    class CustomItemPresenter(presenters.ItemPresenter):
        @property
        def custom_text(self) -> str:
            return "CUSTOMIZED"

    item = FakeItem(id=3, name="bar", completed=False)
    presenter = CustomItemPresenter(item)
    assert presenter.text == "  [3] bar CUSTOMIZED"


def test_item_list_presenter():
    item1 = FakeItem(id=1, name="hello", completed=True)
    item2 = FakeItem(id=2, name="foo", completed=False)
    item3 = FakeItem(id=3, name="bar", completed=False)

    item1_text = presenters.ItemPresenter(item1).text
    item2_text = presenters.ItemPresenter(item2).text
    item3_text = presenters.ItemPresenter(item3).text

    presenter = presenters.ItemListPresenter(items=[item1, item2, item3])
    assert presenter.text == f"{item1_text}\n{item2_text}\n{item3_text}"


def test_item_list_presenter_when_list_is_empty():
    presenter = presenters.ItemListPresenter(items=[])
    assert presenter.text == presenters.DEFAULT_EMPTY_LIST_MESSAGE


def test_item_list_presenter_when_list_is_empty_with_custom_message():
    presenter = presenters.ItemListPresenter(
        items=[], empty_list_message="All fooed up."
    )
    assert presenter.text == "All fooed up."


def test_item_list_presenter_with_custom_item_presenter_class():
    class CustomItemPresenter(presenters.ItemPresenter):
        @property
        def custom_text(self) -> str:
            return "SPRING BREAK"

    item = FakeItem(id=1, name="fooboo")
    presenter = presenters.ItemListPresenter(
        items=[item], item_presenter_class=CustomItemPresenter
    )
    assert presenter.text == CustomItemPresenter(item).text
