import json

from gpg_git_store import StoreFile


def test_store_file_data_returns_dict_of_json_file_data(item_file_path):
    item_data = {"id": 3, "name": "foobar", "foo": True}
    item_file_path.write_text(json.dumps(item_data))
    assert StoreFile(item_file_path).data == {"id": 3, "name": "foobar", "foo": True}


def test_store_file_data_uses_encrypter_to_read_file_data_if_provided(
    fake_encrypter, item_file_path
):
    fake_encrypter.decrypted_data = json.dumps({"id": 1, "name": "foo", "foo": True})
    item_file_path.write_text("h4x0r5")
    assert StoreFile(
        path=item_file_path,
        encrypter=fake_encrypter,
    ).data == {"id": 1, "name": "foo", "foo": True}


def test_store_file_delete_removes_file(item_file_path):
    item_file_path.write_text("")

    store_file = StoreFile(item_file_path)
    store_file.delete()

    assert not item_file_path.exists()


def test_store_file_save_updates_file_with_data_provided(item_file_path):
    original_data = {"id": -1, "name": "heelo", "foo": False}
    item_file_path.write_text(json.dumps(original_data))

    store_file = StoreFile(item_file_path)
    new_data = {"id": 1, "name": "hello", "foo": False}
    store_file.save(new_data)
    assert json.loads(item_file_path.read_text()) == new_data


def test_store_file_save_encrypts_file_data_if_encrypter_provider(
    fake_encrypter, item_file_path
):
    fake_encrypter.encrypted_data = "h4x0r5"
    store_file = StoreFile(path=item_file_path, encrypter=fake_encrypter)
    new_data = {"id": 1, "name": "hello", "foo": False}
    store_file.save(new_data)
    assert item_file_path.read_text() == "h4x0r5"
