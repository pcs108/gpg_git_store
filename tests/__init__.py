import dataclasses
import typing
from pathlib import Path

from gpg_git_store import RelatedItems, StoreItem

TESTS_ROOT = Path(__file__).parent
TEST_DATA = TESTS_ROOT / ".data"
STORE_PATH = TEST_DATA / ".store"


@dataclasses.dataclass
class Relative(StoreItem):
    pass


@dataclasses.dataclass
class AppStoreItem(StoreItem):
    foo: bool = False
    relatives: RelatedItems = RelatedItems(Relative)


@dataclasses.dataclass(slots=True)
class FakeEncrypter:
    gpg_key: typing.Optional[str] = None

    decrypted_data: typing.Optional[dict] = None
    encrypted_data: typing.Optional[dict] = None

    def decrypt(self, *args, **kwargs) -> dict:
        if not self.decrypted_data:
            raise AttributeError("Did not provide decrypted data")
        return self.decrypted_data

    def encrypt(self, *args, **kwargs) -> dict:
        if not self.encrypted_data:
            raise AttributeError("Did not provide encrypted data")
        return self.encrypted_data


@dataclasses.dataclass(slots=True)
class FakeGitter:
    adds: int = 0
    commits: typing.List[str] = dataclasses.field(default_factory=list)

    def add_changes_and_commit(self, message: str) -> None:
        self.adds += 1
        self.commits.append(message)


@dataclasses.dataclass(slots=True)
class FakeStore:
    gitter: typing.Optional[FakeGitter] = None


@dataclasses.dataclass(slots=True)
class FakeItem:
    id: int
    name: str
    completed: bool = False


@dataclasses.dataclass(slots=True)
class FakeCompletedProcess:
    returncode: int = 0
    stdout: bytes = b""
    stderr: bytes = b""


@dataclasses.dataclass(slots=True)
class FakeClickCommand:
    name: str


@dataclasses.dataclass(slots=True)
class FakeClickContext:
    class Aborted(Exception):
        pass

    command: typing.Optional[FakeClickCommand] = None
    obj: typing.Any = None

    def abort(self):
        raise FakeClickContext.Aborted()

    @classmethod
    def from_kwargs(cls, command_name: str, **kwargs):
        if command_name:
            command = FakeClickCommand(name=command_name)
        else:
            command = None
        return cls(command=command, **kwargs)
