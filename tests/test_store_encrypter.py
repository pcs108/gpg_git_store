import json

import pytest

from gpg_git_store import Store, StoreEncrypter

from . import AppStoreItem


def test_store_encrypter_encrypt_sets_gpg_id_and_replaces_plain_text_files(
    fake_encrypter, created_store_path
):
    fake_encrypter.gpg_key = "foo@bar"
    fake_encrypter.encrypted_data = "h4x0r5"

    item_file_path = created_store_path / "abc123"
    item_file_path.write_text(json.dumps({"id": 1, "name": "foo", "foo": True}))

    store = Store(root=created_store_path, item_class=AppStoreItem)

    store_encrypter = StoreEncrypter(store=store, encrypter=fake_encrypter)
    store_encrypter.encrypt()

    assert (created_store_path / ".gpg-id").read_text() == "foo@bar"
    assert item_file_path.read_text() == "h4x0r5"


def test_store_encrypter_encrypt_does_not_set_gpg_id_if_for_related_items(
    fake_encrypter, created_store_path
):
    fake_encrypter.gpg_key = "foo@bar"
    fake_encrypter.encrypted_data = "h4x0r5"

    item_file_path = created_store_path / "abc123"
    item_file_path.write_text(json.dumps({"id": 1, "name": "foo", "foo": True}))

    store = Store(root=created_store_path, item_class=AppStoreItem)

    store_encrypter = StoreEncrypter(
        store=store, encrypter=fake_encrypter, related_items=True
    )
    store_encrypter.encrypt()

    assert not (created_store_path / ".gpg-id").exists()


def test_store_encrypter_encrypt_raises_error_if_store_already_encrypted(
    fake_encrypter, created_store_path
):
    (created_store_path / ".gpg-id").write_text("foo@boo")
    store = Store(root=created_store_path, item_class=AppStoreItem)

    store_encrypter = StoreEncrypter(store=store, encrypter=fake_encrypter)
    with pytest.raises(StoreEncrypter.AlreadyEncryptedError):
        store_encrypter.encrypt()
