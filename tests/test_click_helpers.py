import pytest

from gpg_git_store.click import State, helpers

from . import FakeClickContext, FakeGitter, FakeStore

GITTER = "GITTER"
STORE = "STORE"


@helpers.require_store
def store_required(*args, **kwargs):
    return True


@helpers.save_git_changes
def git_changes_saved(*args, **kwargs):
    return True


def test_abort_if_false_passes_when_not_false():
    ctx = FakeClickContext()
    # Does not abort.
    helpers.abort_if_false(ctx, None, True)


def test_abort_if_false_aborts_when_false():
    ctx = FakeClickContext()
    with pytest.raises(FakeClickContext.Aborted):
        helpers.abort_if_false(ctx, None, False)


def test_require_store_passes_when_store_exists():
    ctx = FakeClickContext(obj=State(store=STORE))
    assert store_required(ctx) is True


def test_require_store_aborts_when_store_does_not_exist():
    ctx = FakeClickContext(obj=State(store=None))
    with pytest.raises(FakeClickContext.Aborted):
        store_required(ctx)


def test_save_git_changes():
    gitter = FakeGitter()
    ctx = FakeClickContext.from_kwargs(
        command_name="hello",
        obj=State(
            store=FakeStore(gitter=gitter),
            git_commit_messages_for_commands={"hello": "world"},
        ),
    )
    assert git_changes_saved(ctx) is True
    assert gitter.adds == 1
    assert gitter.commits == ["world"]


def test_save_git_changes_raises_error_when_command_not_recognized():
    ctx = FakeClickContext.from_kwargs(
        command_name="foo",
        obj=State(
            store=FakeStore(gitter=GITTER),
            git_commit_messages_for_commands={"hello": "world"},
        ),
    )
    with pytest.raises(helpers.CommandNotRecognizedForGitCommit):
        git_changes_saved(ctx)
