from gpg_git_store import ItemListFilter

from . import FakeItem

ITEM = "ITEM"


def test_filter_from_kwargs_assigns_criteria():
    filter = ItemListFilter.from_kwargs(items=[ITEM], hello="world", foo=True)
    assert filter == ItemListFilter(
        items=[ITEM], criteria={"hello": "world", "foo": True}
    )


def test_filter_results_for_single_criterion():
    item1 = FakeItem(id=1, name="hello", completed=True)
    item2 = FakeItem(id=2, name="foo", completed=False)
    item3 = FakeItem(id=3, name="bar", completed=True)
    filter = ItemListFilter(items=[item1, item2, item3], criteria={"completed": True})
    assert filter.results == [item1, item3]


def test_filter_results_for_multiple_criteria():
    item1 = FakeItem(id=1, name="hello", completed=True)
    item2 = FakeItem(id=2, name="foo", completed=False)
    item3 = FakeItem(id=3, name="bar", completed=True)
    filter = ItemListFilter(
        items=[item1, item2, item3], criteria={"completed": True, "name": "bar"}
    )
    assert filter.results == [item3]
