import json
from unittest.mock import call

import pytest

from gpg_git_store import Encrypter, Gitter, Store, StoreFile

from . import AppStoreItem


def test_store_get_file_name_for_id_returns_hash_generated_from_id():
    assert Store.get_file_name_for_id(111) == "698d51a19d8a121ce581499d7b701668"


def test_store_sets_convenience_methods_for_item_interactions(created_store_path):
    store = Store(root=created_store_path, item_class=AppStoreItem)

    assert store.app_store_items == store.items
    assert store.app_store_item_list == store.item_list
    assert store.create_app_store_item == store.create_item


def test_store_init_creates_store_directory(store_path):
    Store.init(path=store_path, item_class=AppStoreItem)
    assert store_path.exists()


def test_store_init_configures_gpg_id_if_specified(store_path):
    Store.init(path=store_path, item_class=AppStoreItem, gpg_id="hello@world")
    assert (store_path / ".gpg-id").read_text() == "hello@world"


def test_store_init_sets_up_git_if_specified(mocked_subprocess_run, store_path):
    Store.init(path=store_path, item_class=AppStoreItem, git=True)
    mocked_subprocess_run.assert_has_calls(
        [
            call("git init", cwd=str(store_path), shell=True),
            call("git add .", cwd=str(store_path), shell=True),
            call("git commit -m 'Store created.'", cwd=str(store_path), shell=True),
        ]
    )


def test_store_init_raises_error_if_specified_store_directory_exists(
    created_store_path,
):
    with pytest.raises(FileExistsError):
        Store.init(path=created_store_path, item_class=AppStoreItem)


def test_store_from_directory(created_store_path):
    assert Store.from_directory(created_store_path, item_class=AppStoreItem) == Store(
        root=created_store_path, item_class=AppStoreItem
    )


def test_store_from_directory_sets_encrypter_if_gpg_id_is_configured(
    created_store_path,
):
    (created_store_path / ".gpg-id").write_text("hello@world")
    assert Store.from_directory(created_store_path, item_class=AppStoreItem) == Store(
        root=created_store_path,
        item_class=AppStoreItem,
        encrypter=Encrypter("hello@world"),
    )


def test_store_directory_sets_gitter_if_git_is_initialized(created_store_path):
    (created_store_path / ".git").mkdir()
    assert Store.from_directory(created_store_path, item_class=AppStoreItem) == Store(
        root=created_store_path,
        item_class=AppStoreItem,
        gitter=Gitter(created_store_path),
    )


def test_store_from_directory_raises_error_if_directory_does_not_exist(store_path):
    with pytest.raises(FileNotFoundError):
        Store.from_directory(store_path, item_class=AppStoreItem)


def test_store_item_file_paths_returns_list_of_item_file_paths_in_root_dir(
    created_store_path,
):
    item_file_path1 = created_store_path / "abc123"
    item_file_path1.write_text("")

    item_file_path2 = created_store_path / "xyx987"
    item_file_path2.write_text("")

    (created_store_path / ".gpg-id").write_text("")
    (created_store_path / ".git").mkdir()
    (created_store_path / "bogus").mkdir()

    store = Store(root=created_store_path, item_class=AppStoreItem)
    assert store.item_file_paths == [item_file_path1, item_file_path2]


def test_store_items_returns_dict_of_ids_mapped_to_items_built_from_files_in_root_dir(
    created_store_path,
):
    item1_path = created_store_path / "abc123"
    item1_data = {"id": 222, "name": "foo", "foo": True}
    item1_path.write_text(json.dumps(item1_data))
    item_file1 = StoreFile(item1_path)

    item2_path = created_store_path / "xyz321"
    item2_data = {"id": 111, "name": "bar", "foo": False}
    item2_path.write_text(json.dumps(item2_data))
    item_file2 = StoreFile(item2_path)

    store = Store(root=created_store_path, item_class=AppStoreItem)
    assert store.items == {
        222: AppStoreItem.from_store_file(item_file1),
        111: AppStoreItem.from_store_file(item_file2),
    }


def test_store_items_decrypts_files_if_encrypter_is_provided(
    fake_encrypter,
    created_store_path,
):
    fake_encrypter.decrypted_data = json.dumps({"id": 963, "name": "hoo", "foo": False})
    item_path = created_store_path / "abc123"
    item_path.write_text("h4x0r5")
    item_file = StoreFile(path=item_path, encrypter=fake_encrypter)

    store = Store(
        root=created_store_path, item_class=AppStoreItem, encrypter=fake_encrypter
    )
    assert store.items == {
        963: AppStoreItem.from_store_file(item_file),
    }


def test_store_item_list_returns_list_of_items(created_store_path):
    item1_path = created_store_path / "abc123"
    item1_data = {"id": 222, "name": "foo", "foo": True}
    item1_path.write_text(json.dumps(item1_data))
    item_file1 = StoreFile(item1_path)

    item2_path = created_store_path / "xyz321"
    item2_data = {"id": 111, "name": "bar", "foo": False}
    item2_path.write_text(json.dumps(item2_data))
    item_file2 = StoreFile(item2_path)

    store = Store(root=created_store_path, item_class=AppStoreItem)
    assert store.item_list == [
        AppStoreItem.from_store_file(item_file2),
        AppStoreItem.from_store_file(item_file1),
    ]


def test_store_is_encrypted_returns_false_if_not_encrypted(created_store_path):
    assert Store(root=created_store_path, item_class=AppStoreItem).is_encrypted is False


def test_store_is_encrypted_returns_true_if_encrypted(created_store_path):
    (created_store_path / ".gpg-id").write_text("foo@boo")
    assert Store(root=created_store_path, item_class=AppStoreItem).is_encrypted is True


def test_store_max_id_starts_with_zero_if_no_items_exist(created_store_path):
    store = Store(root=created_store_path, item_class=AppStoreItem)
    assert store.max_id == 0


def test_store_max_id_picks_out_highest_id_if_items_exist(created_store_path):
    item1_path = created_store_path / "abc123"
    item1_data = {"id": 6, "name": "bar", "foo": False}
    item1_path.write_text(json.dumps(item1_data))

    item2_path = created_store_path / "xyz321"
    item2_data = {"id": 1, "name": "foo", "foo": True}
    item2_path.write_text(json.dumps(item2_data))

    store = Store(root=created_store_path, item_class=AppStoreItem)
    assert store.max_id == 6


def test_store_create_item(created_store_path):
    store = Store(root=created_store_path, item_class=AppStoreItem)
    item_file_path = created_store_path / Store.get_file_name_for_id(321)
    item = store.create_item(id=321, name="fooboo", foo=True)
    assert item == AppStoreItem(
        storage=StoreFile(item_file_path), id=321, name="fooboo", foo=True
    )

    assert json.loads(item_file_path.read_text()) == item.data


def test_store_create_item_assigns_id_if_not_provided_starting_with_one(
    created_store_path,
):
    store = Store(root=created_store_path, item_class=AppStoreItem)
    assert store.create_item(name="hello").id == 1


def test_store_create_item_assigns_id_if_not_provided_incrementing_from_previous_max_id(
    created_store_path,
):
    item_path = created_store_path / "abc123"
    item_path.write_text(json.dumps({"id": 4, "name": "foo", "foo": True}))
    store = Store(root=created_store_path, item_class=AppStoreItem)

    assert store.create_item(name="bar").id == 5


def test_store_create_item_encrypts_file_if_encrypter_is_provided(
    fake_encrypter, created_store_path
):
    fake_encrypter.encrypted_data = "h4x0r5"
    store = Store(
        root=created_store_path, item_class=AppStoreItem, encrypter=fake_encrypter
    )
    item = store.create_item(id=1, name="foo")
    assert item.id == 1
    assert item.storage.path.read_text() == "h4x0r5"


def test_store_unset_gpg_id_removes_key_id_file(created_store_path):
    gpg_id_file_path = created_store_path / ".gpg-id"
    gpg_id_file_path.write_text("boo@boo")

    store = Store(root=created_store_path, item_class=AppStoreItem)
    store.unset_gpg_id()
    assert not gpg_id_file_path.exists()


def test_store_set_gpg_key_writes_key_id_to_file(created_store_path):
    store = Store(root=created_store_path, item_class=AppStoreItem)
    store.set_gpg_id("hello@world")
    assert (created_store_path / ".gpg-id").read_text() == "hello@world"
