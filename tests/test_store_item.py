import json

from gpg_git_store import Store, StoreFile

from . import AppStoreItem, Relative


def test_store_item_from_store_file_constructs_store_item_from_file_data(
    item_file_path,
):
    store_item_data = {"id": 2, "name": "hello", "foo": False}
    item_file_path.write_text(json.dumps(store_item_data))
    store_item_file = StoreFile(item_file_path)
    assert AppStoreItem.from_store_file(store_item_file) == AppStoreItem(
        storage=store_item_file, **store_item_data
    )


def test_store_item_can_reference_related_items(item_file_path):
    store_item = AppStoreItem(
        storage=StoreFile(item_file_path), id=111, name="foobar", foo=True
    )
    brother_data = {"id": 1, "name": "brother"}
    brother_path = store_item.relatives.store.root / "abc123"
    brother_file = StoreFile(path=brother_path)
    brother_file.save(brother_data)
    assert store_item.relatives.items == {
        1: Relative(id=1, name="brother", storage=brother_file)
    }


def test_store_item_can_create_related_item(store_path, item_file_path):
    store_item = AppStoreItem(
        storage=StoreFile(item_file_path), id=111, name="foobar", foo=True
    )
    brother = store_item.relatives.create_item(id=1, name="brother")
    brother_path = (
        store_path / store_item.relatives.get_directory_name_for_id(111)
    ) / Store.get_file_name_for_id(1)
    assert brother == Relative(
        id=1, name="brother", storage=StoreFile(path=brother_path)
    )


def test_store_item_can_delete_related_items(created_store_path, item_file_path):
    store_item = AppStoreItem(
        storage=StoreFile(item_file_path), id=111, name="foobar", foo=True
    )
    brother_data = {"id": 1, "name": "brother"}
    brother_path = store_item.relatives.store.root / "abc123"
    brother_file = StoreFile(path=brother_path)
    brother_file.save(brother_data)

    store_item.relatives.items[1].delete()

    assert store_item.relatives.items == {}
    assert not brother_path.exists()


def test_store_item_data_property_returns_dict_of_store_item_item_data(item_file_path):
    assert AppStoreItem(
        storage=StoreFile(item_file_path), id=111, name="foobar", foo=True
    ).data == {
        "id": 111,
        "name": "foobar",
        "foo": True,
    }


def test_store_item_delete_removes_file(item_file_path):
    store_item_data = {"id": 1, "name": "foo", "foo": True}
    item_file_path.write_text(json.dumps(store_item_data))

    store_item = AppStoreItem(storage=StoreFile(item_file_path), **store_item_data)
    store_item.delete()

    assert not item_file_path.exists()


def test_store_item_delete_removes_related_items(store_path, item_file_path):
    item_file_path.write_text("")
    store_item = AppStoreItem(storage=StoreFile(item_file_path), id=1, name="foo")
    store_item.relatives.create_item(name="fish")
    store_item.delete()

    assert list(store_path.iterdir()) == []


def test_store_item_save_persists_store_item_state(item_file_path):
    store_item = AppStoreItem(
        storage=StoreFile(item_file_path), id=6, name="goodbye", foo=False
    )
    store_item.save()
    assert json.loads(item_file_path.read_text()) == store_item.data


def test_store_item_update_changes_attributes_on_store_item(item_file_path):
    store_item = AppStoreItem(
        storage=StoreFile(item_file_path), id=3, name="foo", foo=True
    )
    store_item.update(name="bar", foo=False)
    assert store_item.name == "bar"
    assert store_item.foo is False
