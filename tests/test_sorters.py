from gpg_git_store import ItemListSorter

from . import FakeItem


def test_sorter_sorts_items_by_specified_attribute():
    item1 = FakeItem(id=1, name="c")
    item2 = FakeItem(id=2, name="a")
    item3 = FakeItem(id=3, name="b")

    sorter = ItemListSorter(items=[item1, item2, item3], attribute="name")
    assert sorter.results == [item2, item3, item1]


def test_sorter_reverses_order_when_specified():
    item1 = FakeItem(id=1, name="c")
    item2 = FakeItem(id=2, name="a")
    item3 = FakeItem(id=3, name="b")

    sorter = ItemListSorter(items=[item1, item2, item3], attribute="name", reverse=True)
    assert sorter.results == [item1, item3, item2]
