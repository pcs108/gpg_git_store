import json
from unittest.mock import call

import click
import pytest
from click.testing import CliRunner

from gpg_git_store import Gitter, Store
from gpg_git_store.click import State, commands

from . import STORE_PATH, AppStoreItem, FakeCompletedProcess


@click.group()
@click.pass_context
def cli(ctx: click.Context) -> None:
    if STORE_PATH.exists():
        store = Store.from_directory(STORE_PATH, item_class=AppStoreItem)
    else:
        store = None

    ctx.obj = State(store=store, store_item_class=AppStoreItem, store_path=STORE_PATH)


cli.command(commands.init)
cli.command(commands.decrypt)
cli.command(commands.encrypt)
cli.command(commands.git)


@pytest.fixture
def runner():
    return CliRunner()


def test_cli_init_creates_store_directory(store_path, runner):
    result = runner.invoke(cli, ["init"], input="\n")
    assert result.exit_code == 0

    assert store_path.exists()


def test_cli_init_sets_gpg_id_if_specified(store_path, runner):
    result = runner.invoke(cli, ["init", "--gpg-key", "hello@goodbye"])
    assert result.exit_code == 0

    assert (store_path / ".gpg-id").exists()


def test_cli_init_inits_git_if_specified(mocked_subprocess_run, store_path, runner):
    result = runner.invoke(cli, ["init", "--git-init"], input="\n")
    assert result.exit_code == 0

    mocked_subprocess_run.assert_has_calls(
        [
            call(Gitter.INIT_COMMAND, shell=True, cwd=str(store_path)),
            call(Gitter.ADD_COMMAND, shell=True, cwd=str(store_path)),
            call(
                Gitter.COMMIT_COMMAND.format(message="Store created."),
                shell=True,
                cwd=str(store_path),
            ),
        ]
    )


def test_cli_decrypt_decrypts_the_store_when_confirmed(
    created_store_path, mocked_subprocess_run, runner
):
    (created_store_path / ".gpg-id").write_text("fabio@fooboo")
    todo_data = {"id": 1, "name": "fabio", "completed": True, "priority": 967}
    mocked_subprocess_run.return_value = FakeCompletedProcess(
        stdout=json.dumps(todo_data).encode()
    )

    result = runner.invoke(cli, ["decrypt"], input="y\n")
    assert result.exit_code == 0

    assert result.output == "Decrypt the store? [y/N]: y\nStore was decrypted.\n"


def test_cli_decrypt_aborts_when_not_confirmed(
    created_store_path, mocked_subprocess_run, runner
):
    (created_store_path / ".gpg-id").write_text("fabio@fooboo")
    todo_data = {"id": 1, "name": "fabio", "completed": True, "priority": 967}
    mocked_subprocess_run.return_value = FakeCompletedProcess(
        stdout=json.dumps(todo_data).encode()
    )

    result = runner.invoke(cli, ["decrypt"], input="n\n")
    assert result.exit_code == 1

    assert result.output == "Decrypt the store? [y/N]: n\nAborted!\n"


def test_cli_decrypt_skips_confirmation_when_flagged(
    created_store_path, mocked_subprocess_run, runner
):
    (created_store_path / ".gpg-id").write_text("fabio@fooboo")
    todo_data = {"id": 1, "name": "fabio", "completed": True, "priority": 967}
    mocked_subprocess_run.return_value = FakeCompletedProcess(
        stdout=json.dumps(todo_data).encode()
    )

    result = runner.invoke(cli, ["decrypt", "--force"])
    assert result.exit_code == 0


def test_cli_encrypt_encrypts_the_store_when_confirmed(
    created_store_path, mocked_subprocess_run, runner
):
    mocked_subprocess_run.return_value = FakeCompletedProcess(stdout=b"h4x0r5")
    result = runner.invoke(cli, ["encrypt", "hello@world"], input="y\n")
    assert result.exit_code == 0

    assert (
        result.output == "Encrypt the store? [y/N]: y\nEncrypted with: hello@world.\n"
    )


def test_cli_encrypt_aborts_when_not_confirmed(created_store_path, runner):
    result = runner.invoke(cli, ["encrypt", "hello@world"], input="n\n")
    assert result.exit_code == 1

    assert result.output == "Encrypt the store? [y/N]: n\nAborted!\n"


def test_cli_encrypt_skips_confirmation_when_flagged(
    created_store_path, mocked_subprocess_run, runner
):
    mocked_subprocess_run.return_value = FakeCompletedProcess(stdout=b"h4x0r5")
    result = runner.invoke(cli, ["encrypt", "hello@world", "--force"])
    assert result.exit_code == 0


def test_cli_git_runs_git_command(mocked_subprocess_run, created_store_path, runner):
    (created_store_path / ".git").mkdir()
    result = runner.invoke(cli, ["git", "status"])
    assert result.exit_code == 0

    mocked_subprocess_run.assert_called_with(
        "git status", cwd=str(created_store_path), shell=True
    )


def test_cli_git_raises_error_if_git_not_set_up(created_store_path, runner):
    result = runner.invoke(cli, ["git", "status"])
    assert result.exit_code == 1
    assert "not set up " in result.output
